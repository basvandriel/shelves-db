# shelves
[![Build Status](https://img.shields.io/travis/basvandriel/shelves/master.svg?style=flat-square)](https://travis-ci.org/basvandriel/on-track)
[![Coverage Status](https://img.shields.io/coveralls/basvandriel/shelves.svg?style=flat-square)](https://coveralls.io/github/basvandriel/shelves)
[![License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](LICENSE.md)

This repository contains a simple and easy database
wrapper made in PHP for security and ease of use.

I created this project because I wanted to learn more about object-oriented
principles in PHP. I also wanted to learn more about Test Driven Development.

There are currently only MySQL database connections supported.

## Requirements
* The `git` tool
* A project using PHP >= 7.1
* A running MySQL server
* The `composer` dependency manager tool

## Usage
Create a `ConnectionBuilder` object with the `driver` you want to use.
Currently only the `mysql` driver is supported.
  
Then call the `build()` method, with an array of connection credentials.

The credentials you can use are:
 - `host` - The host where the database is located *
 - `port` - The port number where the database is located
 - `database` - The database name *
 - `username` - The user for the database *
 - `password` - The password for the username 
 - `charset` - The charset to use for the database

\* _means the field is required_


```php
# Builds the database connection
$databaseConnection = (new ConnectionBuilder('mysql'))->build(
   $connectionCredentials
);

# The database credentials
$connectionCredentials = array(
    'driver'   => 'mysql',
    'host'     => '127.0.0.1',
    'username' => 'root',
);
```

Afterwards create a `Database` object and inject the `DatabaseConnection` object.
```php
# The database object
$database = new Database($databaseConnection);
```

### Statements
To run statements, simply use the `execute` method with the desired statement:
```php
$result = $database->execute('...');
```

This will return a `PDOStatement` object where you can fetch data from.

### Prepared statements
To run prepared statements, simply use the `executePrepared` method with also an array of parameters:

```php
# Set the parameters
$parameters = array(
    'name' => 'Bas'
);

$result = $database->executePrepared(
    'SELECT * FROM `users` WHERE name = :name', 
    $parameters
);
```

This will also return a `PDOStatement` object where you can fetch data from.

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Authors
This project was initially created by [Bas van Driel](https://github.com/basvandriel "GitHub page") ([@bvandriel](https://twitter.com/bvandriel "Twitter page")), where [these people](https://github.com/basvandriel/WWW/graphs/contributors) contributed to it.

## Links
* [Source code](https://github.com/basvandriel/shelves)
* [Issue tracker](https://github.com/basvandriel/shelves/issues)