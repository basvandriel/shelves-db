<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */
    use basvandriel\Shelves\Database\Database;

    /**
     * TODO Document the purpose of the DatabaseTest class
     *
     * @package   basvandriel\Shelves\Connection
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class DatabaseTest extends \basvandriel\Shelves\Database\AbstractDatabaseTestCase
    {

        /**
         * @var Database $instance The database instance to test
         */
        private $instance;

        /**
         *
         */
        public function setUp()
        {
            # Call the parent setup
            parent::setUp();

            $pdo = $this->getConnection()
                        ->getConnection();

            $this->instance = new Database($pdo);
        }

        /**
         * A bad connection was made
         */
        public function testIsBadPDOConnection()
        {
            $this->expectException("\PDOException");

            $badPDOConnection = new PDO(
                'mysql:host=localhost;port=3306', 'rood', 'awrongpassword'
            );

            $database = new Database($badPDOConnection);
        }

        /**
         *
         */
        public function testSelectBadConnection()
        {
            $this->expectException(PDOException::class);

            $badPDOConnection = new PDO(
                'mysql:host=localhost;port=3306', 'rood', 'awrongpassword'
            );

            $this->instance->selectConnection($badPDOConnection);
        }

        /**
         *
         */
        public function testExecuteStatement()
        {
            $expected = array(
                'id'   => "1",
                0      => "1",
                'name' => "Bas",
                1      => "Bas"
            );

            $statement = $this->instance->execute('SELECT * FROM `users` WHERE `id` = 1');
            $actualResult = $statement->fetch();

            $this->assertEquals($expected, $actualResult);
        }

        /**
         *
         */
        public function testExecutePreparedStatement()
        {
            $expected = array(
                'id'   => "1",
                0      => "1",
                'name' => "Bas",
                1      => "Bas"
            );

            $statement = $this->instance->executePrepared(
                'SELECT * FROM `users` WHERE `id` = :id', array(
                                                            'id' => '1'
                                                        )
            );

            $actualResult = $statement->fetch();

            $this->assertEquals($expected, $actualResult);
        }

        /**
         *
         */
        public function testDeleteFrom()
        {

            $actual = $this->instance->deleteFrom('users')
                                     ->where('id', '=', '1')
                                     ->where('id', '=', '2', "OR")
                                     ->where('id', '=', '3', "OR")
                                     ->getPDOStatement()
                                     ->rowCount();
            $expected = 3;

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testUpdate()
        {
            $actual = $this->instance->update('users', array('name' => 'Kaka'))
                                     ->where('id', '=', '1')
                                     ->where('id', '=', '2', 'OR')
                                     ->getPDOStatement()
                                     ->rowCount();
            $expected = 2;

            $this->assertEquals($expected, $actual);

        }

        /**
         * SQLite doesn't support this, can't execute query
         */
        public function testUpdateWithDataTablePrefix()
        {
            # Expected query
            $expected = "UPDATE `users` SET `users`.`name` = :usersName WHERE `id` = :id";

            # Some query data
            $updateValues = array(
                'users.name' => 'Kaka'
            );

            # Build the query
            $tableOptionsBuilder = $this->instance->update('users', $updateValues)
                                                  ->where('id', '=', '1');

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($tableOptionsBuilder);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');
            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($tableOptionsBuilder, false);

            # Get the PDO statement so it get generated
            $tableOptionsBuilder->getPDOStatement();

            # Get the statement text property from the table options builders
            $reflectionCurrentStatement = $reflectionClass->getParentClass()
                                                          ->getProperty('currentStatement');
            $reflectionCurrentStatement->setAccessible(true);

            /** @var \basvandriel\Shelves\Database\Statement\Statement $actual */
            $actual = ($reflectionCurrentStatement->getValue($tableOptionsBuilder))->getStatementText();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testUpdateWithNoData()
        {
            $actual = $this->instance->update('users', array())
                                     ->getPDOStatement()->queryString;

            $this->assertEmpty($actual);
        }

        /**
         *
         */
        public function testSelfExecuteOnDestroy()
        {
            $this->instance->deleteFrom('users')
                           ->where('id', '>', '0');

            $actual = $this->instance->execute("SELECT count(*) FROM `users`")
                                     ->fetch()[0];

            $this->assertEquals(0, $actual);
        }

        /**
         * Returns the test dataset.
         *
         * @return \PHPUnit\DbUnit\DataSet\IDataSet
         */
        protected function getDataSet()
        {
            return $this->createXMLDataSet(dirname(__FILE__) . '/test-dataset.xml');
        }
    }
