<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database;


    /**
     * TODO Document the purpose of the ConnectionBuilderTest class
     *
     * @package   basvandriel\Shelves\Database
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class ConnectionBuilderTest extends \PHPUnit\Framework\TestCase
    {
        /**
         */
        public function testHasNoDriver()
        {
            $this->expectException(\PDOException::class);

            $object = new ConnectionBuilder("");
        }

        public function testHasDriver()
        {
            $object = new ConnectionBuilder(
                "mysql"
            );

            $this->assertNotNull($object);
        }

        /**
         * @expectedExceptionMessage The host must be set
         */
        public function testBuildWithNoHost()
        {
            $this->expectException(\PDOException::class);

            $object = new ConnectionBuilder(
                "mysql"
            );

            $object->build(array());
        }

        /**
         * @expectedExceptionMessage The username must be set
         */
        public function testBuildWithNoUsername()
        {
            $this->expectException(\PDOException::class);

            $object = new ConnectionBuilder(
                "mysql"
            );

            $object->build(array('host' => "localhost"));
        }

        /**
         * @expectedExceptionMessage The database must be set
         */
        public function testBuildWithNoDatabase()
        {
            $this->expectException(\PDOException::class);

            $object = new ConnectionBuilder(
                "mysql"
            );

            $object->build(array('host' => "localhost", 'username' => 'bla'));
        }

        /**
         * @expectedException \PDOException
         */
        public function testBuildIsPDO()
        {
            $object = new ConnectionBuilder(
                "mysql"
            );

            $pdo = $object->build(array('host' => "localhost", 'username' => 'root', 'database' => 'test'));
            $this->assertTrue($pdo instanceof \PDO);
        }
    }
