<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database;

    use PDO;

    /**
     * TODO Document the purpose of the AbstractDatabaseTestCase class
     *
     * @package   basvandriel\Shelves\Database
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    abstract class AbstractDatabaseTestCase extends \PHPUnit\Framework\TestCase
    {
        /**
         * Use the DbUnit TestCaseTrait
         */
        use \PHPUnit\DbUnit\TestCaseTrait;

        /**
         * @var \PDO $PDO The PDO connection object
         */
        private static $PDO;

        /**
         * @var \PHPUnit\DbUnit\Database\Connection $connection The DbUnit connection
         */
        private $connection;

        /**
         * Returns the test database connection.
         *
         * @return \PHPUnit\DbUnit\Database\Connection
         */
        protected final function getConnection()
        {
            if (self::$PDO == null) {
                # Set the PDO
                self::$PDO = $this->createInMemoryDatabase();
            }

            return $this->connection = $this->createDefaultDBConnection(self::$PDO, ':memory:');
        }

        /**
         * Creates the in memory database
         *
         * @return \PDO The PDO connection object
         */
        private function createInMemoryDatabase() : PDO
        {
            $PDO = new PDO('sqlite::memory:');
            $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $PDO->exec("CREATE TABLE users (id INT PRIMARY KEY, name VARCHAR(50))");

            return $PDO;
        }
    }
