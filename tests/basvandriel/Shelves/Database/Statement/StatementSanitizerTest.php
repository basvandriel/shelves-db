<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement;
    /**
     * Tests the StatementSanitizerTest class
     *
     * @package   basvandriel\Shelves\Database\Statement
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class StatementSanitizerTest extends \PHPUnit\Framework\TestCase
    {
        /**
         * @var \basvandriel\Shelves\Database\Statement\StatementSanitizer $instance
         */
        private $instance;

        /**
         * @covers \basvandriel\Shelves\Database\Statement\StatementSanitizer::__construct()
         */
        public function setUp()
        {
            $this->instance = new StatementSanitizer();
        }


        /**
         * @covers \basvandriel\Shelves\Database\Statement\StatementSanitizer::sanitizeStatement()
         */
        public function testStringWithSpacesAtSides()
        {
            $expected = "SELECT * FROM `users`";
            $input = "   SELECT * FROM `users`     ";
            $actual = $this->instance->sanitizeStatement($input);

            $this->assertEquals($expected, $actual);
        }
    }
