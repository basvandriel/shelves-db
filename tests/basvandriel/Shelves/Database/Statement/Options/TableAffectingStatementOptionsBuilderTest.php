<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\Options;

    use basvandriel\Shelves\Database\Database;
    use basvandriel\Shelves\Database\Statement\Statement;
    use PDO;
    use ReflectionClass;

    /**
     * TODO Document the purpose of the TableAffectingStatementOptionsBuilderTest class
     *
     * @package   basvandriel\Shelves\Database\Statement\Options
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class TableAffectingStatementOptionsBuilderTest extends \PHPUnit\Framework\TestCase
    {
        /**
         * @var \basvandriel\Shelves\Database\Statement\Options\TableAffectingStatementOptionsBuilder $instance
         */
        private $instance;

        /**
         *
         */
        public function setUp()
        {
            $PDO = new PDO('sqlite::memory:');
            $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $PDO->exec("CREATE TABLE users (id INT PRIMARY KEY, name VARCHAR(50))");
            $PDO->exec("INSERT INTO `users` (id, name) VALUES (1, 'Bas')");
            $PDO->exec("INSERT INTO `users` (id, name) VALUES (2, 'Max')");

            $database = new Database($PDO);
            $statement = new Statement('DELETE FROM `users`');

            # Use a delete from statement
            $this->instance = new TableAffectingStatementOptionsBuilder(
                $database, $statement, array(), 'users'
            );
        }

        /**
         *
         */
        public function tearDown()
        {
            unset($this->instance);
        }

        /**
         *
         */
        public function testIsInstanceOfParent()
        {
            $this->assertInstanceOf(TableAffectingStatementOptionsBuilder::class, $this->instance);
        }

        /**
         *
         */
        public function testPrefixedWhere()
        {

            $actual = $this->instance->where('users.id', '=', '1')
                                     ->getPDOStatement()->queryString;

            $expected = "DELETE FROM `users` WHERE `users`.`id` = :usersId;";

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testWhereWithIncorrectANDorOR()
        {

            $actual = $this->instance->where('id', '=', '1')
                                     ->where('id', '=', '2', "ZOR")
                                     ->getPDOStatement()->queryString;

            $expected = "DELETE FROM `users` WHERE `id` = :id;";

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testShouldNotExecute()
        {
            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');
            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create some query data and return the query string
            $actual = $this->instance->where('id', ' = ', '1')
                                     ->where('name', ' = ', 'Bas')
                                     ->getPDOStatement()->queryString;

            $this->assertNull($actual);
        }
    }
