<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\Options;

    use basvandriel\Shelves\Database\Database;
    use basvandriel\Shelves\Database\Statement\Statement;
    use PDO;
    use ReflectionClass;

    /**
     * TODO Document the purpose of the StatementOptionsBuilderTest class
     *
     * @package   basvandriel\Shelves\Database\Statement\Options
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class StatementOptionsBuilderTest extends \PHPUnit\Framework\TestCase
    {
        /**
         * @var \basvandriel\Shelves\Database\Statement\Options\StatementOptionsBuilder $instance
         */
        private $instance;

        /**
         *
         */
        public function setUp()
        {
            $PDO = new PDO('sqlite::memory:');
            $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $database = new Database($PDO);
            $statement = new Statement('DELETE FROM `users`');

            # Use a delete from statement
            $this->instance = new TableAffectingStatementOptionsBuilder(
                $database, $statement, array(), 'users'
            );
        }

        /**
         *
         */
        public function tearDown()
        {
            unset($this->instance);
        }

        /**
         *
         */
        public function testSingleInnerJoin()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` ON `id` = `emailaddresses`.`id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'id', '=', 'id')
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testSingleInnerJoinWithSelfPrefixOnConnectingTableField()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` ON `id` = `emailaddresses`.`id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'id', '=', 'emailaddresses.id')
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testSingleInnerJoinWithOtherPrefixOnConnectingTableField()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` ON `id` = `random`.`id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'id', '=', 'random.id')
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testSingleInnerJoinWithBadJoinType()
        {
            $expected = "DELETE FROM `users`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'id', '=', 'random.id', "RANDOM")
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testSingleInnerJoinWithPrefixingConnectingBaseTableField()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` ON `users`.`id` = `emailaddresses`.`users_id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'users.id', '=', 'users_id')
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testSingleInnerJoinWithNoPrefixingConnectingBaseTableField()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` ON `id` = `emailaddresses`.`users_id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'id', '=', 'users_id')
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testRightJoin()
        {
            $expected = "DELETE FROM `users` RIGHT JOIN `emailaddresses` ON `users`.`id` = `emailaddresses`.`users_id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'users.id', '=', 'users_id', "RIGHT")
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testLeftJoin()
        {
            $expected = "DELETE FROM `users` LEFT JOIN `emailaddresses` ON `users`.`id` = `emailaddresses`.`users_id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'users.id', '=', 'users_id', "LEFT")
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testFullOuterJoin()
        {
            $expected =
                "DELETE FROM `users` FULL OUTER JOIN `emailaddresses` ON `users`.`id` = `emailaddresses`.`users_id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses', 'users.id', '=', 'users_id', "FULL OUTER")
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testSingleInnerJoinWithAlias()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` AS ea ON `id` = ea.`users_id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses:ea', 'id', '=', 'users_id')
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testSingleInnerJoinWithPrefixedAlias()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` AS ea ON users.`id` = ae.`users_id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses:ea', 'users::id', '=', 'ae::users_id')
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testMultipleJoins()
        {
            $expected = "DELETE FROM `users` INNER JOIN `emailaddresses` AS ea ON `users`.`id` = ae.`users_id` INNER JOIN `adresses` AS adr ON ae.`addres_id` = adr.`id`";

            # Set the should execute to false
            $reflectionClass = new ReflectionClass($this->instance);
            $reflectionShouldExecute = $reflectionClass->getParentClass()
                                                       ->getProperty('shouldExecute');

            $reflectionShouldExecute->setAccessible(true);
            $reflectionShouldExecute->setValue($this->instance, false);

            # Create the query here
            $actual = $this->instance->join('emailaddresses:ea', 'users.id', '=', 'ae::users_id')
                                     ->join('adresses:adr', 'ae::addres_id', "=", "adr::id")
                                     ->getStatement();

            $this->assertEquals($expected, $actual);
        }
    }
