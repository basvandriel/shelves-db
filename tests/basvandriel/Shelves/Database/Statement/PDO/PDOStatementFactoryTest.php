<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\PDO;

    use basvandriel\Shelves\Database\Statement\PDOStatementFactory;
    use PDO;

    /**
     * Tests the PDOStatementFactoryTest class
     *
     * @package   basvandriel\Shelves\Database\Statement\PDO
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class PDOStatementFactoryTest extends \PHPUnit\Framework\TestCase
    {

        /**
         * @var \basvandriel\Shelves\Database\Statement\PDOStatementFactory $instance
         */
        private $instance;

        /**
         * @covers \basvandriel\Shelves\Database\Statement\PDOStatementFactory::__construct()
         */
        public function setUp()
        {
            $PDO = new PDO('sqlite::memory:');
            $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $PDO->exec("CREATE TABLE users (id INT PRIMARY KEY, name VARCHAR(50))");
            $this->instance = new PDOStatementFactory($PDO);
        }

        /**
         *
         */
        public function tearDown()
        {
            unset($this->instance);
        }

        /**
         * @covers \basvandriel\Shelves\Database\Statement\PDOStatementFactory::build()
         */
        public function testBuildWithNonPreparedStatement()
        {
            //Expected rows affected to return
            $expectedResult = 0;

            $actualResult = $this->instance->build('DELETE FROM `users`')
                                           ->rowCount();

            $this->assertEquals($expectedResult, $actualResult);
        }

        /**
         * @covers \basvandriel\Shelves\Database\Statement\PDOStatementFactory::buildPrepared()
         */
        public function testBuildPreparedStatementWithIncorrectQuery()
        {
            $parameters = array(
                'name' => 'Bas'
            );

            # Build the prepared statement
            $query = $this->instance->buildPrepared(
                "SELECT `name` FROM `users` WHERE `name` = :noname", $parameters
            );

            $this->assertNull($query->fetch()['name']);
        }

        /**
         * \basvandriel\Shelves\Database\Statement\PDOStatementFactory::buildPrepared()
         * \basvandriel\Shelves\Database\Statement\PDOStatementFactory::mapPreparedStatementParameters()
         */
        public function testBuildPreparedStatementWithCorrectParameters()
        {
            $this->instance->build("INSERT INTO `users` (id, name) VALUES (NULL, 'Bas')");

            $parameters = array(
                'name' => 'Bas'
            );

            # Build the prepared statement
            $query = $this->instance->buildPrepared("SELECT `name` FROM `users` WHERE `name` = :name", $parameters);

            $expected = "Bas";
            $actual = $query->fetch()['name'];

            $this->assertEquals($expected, $actual);
        }

        /**
         *
         */
        public function testBuildPreparedStatementWithParameterNameNotAsString()
        {
            $this->instance->build("INSERT INTO `users` (id, name) VALUES (NULL, 'Bas')");

            $parameters = array(
                123 => 'Bas'
            );

            # Build the prepared statement
            $query = $this->instance->buildPrepared("SELECT `name` FROM `users` WHERE `name` = :name", $parameters);

            $actual = $query->fetch()['name'];

            $this->assertNull($actual);
        }

        /**
         *
         */
        public function testBuildPreparedStatementWithParameterValueAsArray()
        {
            $this->instance->build("INSERT INTO `users` (id, name) VALUES (NULL, 'Bas')");

            $parameters = array(
                'name' => 'bas'
            );

            # Build the prepared statement
            $query = $this->instance->buildPrepared("SELECT `name` FROM `users` WHERE `name` = :name", $parameters);

            $actual = $query->fetch()['name'];

            $this->assertNull($actual);
        }
    }
