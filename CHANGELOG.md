# Changes in shelves
All changes of the shelves library release series are documented in this file using the [Keep A CHANGELOG principles](http://keepachangelog.com/).

All the unreleased changes can be [found here](https://github.com/basvandriel/shelves/compare/1.0.0...HEAD).

## 1.1 (July 12, 2017)
- Delete statements can be build using the `deleteFrom()` function on a `Database` object.
- Update statements can be build using the `update()` function on a `Database` object.
- INNER JOINs can now be used from by using the `join()` method on a `StatementOptionsBuilder` object.
- LEFT JOINs can now be used from by using the `join()` method on a `StatementOptionsBuilder` object.
- RIGHT JOINs can now be used from by using the `join()` method on a `StatementOptionsBuilder` object.
- OUTER JOINs can now be used from by using the `join()` method on a `StatementOptionsBuilder` object.

## 1.0 (July 7, 2017)
- Statements can be executed from the `Database` class using the `execute` method
- Prepared statements can be executed from the `Database` class using the `executedPrepared` method
- Connections can be selected from the `Database` class using the `selectConnection` method.
- Connections can be build the the `ConnectionBuilder` class.