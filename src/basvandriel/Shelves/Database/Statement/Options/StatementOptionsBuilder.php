<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\Options;

    use basvandriel\Shelves\Database\Database;
    use basvandriel\Shelves\Database\Statement\Options\Option\JoinClauseStatementOptionBuilder;
    use basvandriel\Shelves\Database\Statement\Statement;

    /**
     * TODO Document the purpose of the StatementBuilder class
     *
     * @package   basvandriel\Shelves\Statement
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    abstract class StatementOptionsBuilder
    {
        /**
         * @var Database $database The current database
         */
        private $database;

        /**
         * @var array $options The collection with option names and string
         */
        private $options;

        /**
         * @var \basvandriel\Shelves\Database\Statement\Statement $currentStatement The current statement that already
         *      exists
         */
        private $currentStatement;

        /**
         * @var array $requiredOptions The required options for this statement
         */
        private $requiredOptions;

        /**
         * @var bool $shouldExecute An indicator if the query should execute after the method chain
         */
        private $shouldExecute;

        /**
         * StatementOptionsConnector constructor.
         *
         * @param Database                                          $database
         * @param \basvandriel\Shelves\Database\Statement\Statement $current_statement
         * @param array                                             $required_options
         */
        public function __construct(Database $database, Statement $current_statement, $required_options = array())
        {
            #  Set the database
            $this->database = $database;

            # Set the current statement
            $this->currentStatement = $current_statement;

            # Set the options
            $this->options = array();

            # Set the required options
            $this->requiredOptions = $required_options;

            # By standard the query gets executed at the end of the method chain
            $this->shouldExecute = $current_statement->getStatementText() != "";
        }

        /**
         * Use this function to combine rows from two or more tables, based on a related column between them.
         *
         * @param string $table_name The table that is used to combine
         * @param string $connecting_base_table_field
         * @param string $join_comparing_operator
         * @param string $joining_connecting_table_field
         * @param string $INNER_OR_FULL_OUTER_OR_LEFT_OR_RIGHT
         *
         * @return $this
         * @internal param string $INNER_OR_OUTER_LEFT_OR_RIGHT <p>The JOIN type you want to use. It's default value is
         *           an INNER join.</p>
         *                                              <ul>
         *                                              <li>INNER JOIN: Returns records that have matching values in
         *                                              both tables.</li>
         *                                              <li>LEFT JOIN: Return all records from the left table, and the
         *                                              matched records from the right table.</li>
         *                                              <li>RIGHT JOIN: Return all records from the right table, and
         *                                              the matched records from the left table.</li>
         *                                              <li>FULL JOIN: Return all records when there is a match in
         *                                              either left or right table.</li>
         *
         * @internal param string $joining_table_field
         */
        public function join(string $table_name, string $connecting_base_table_field, string $join_comparing_operator, string $joining_connecting_table_field, string $INNER_OR_FULL_OUTER_OR_LEFT_OR_RIGHT = "INNER")
        {
            # Create the array key if it doesn't exist
            if (!isset($this->getOptions()['JOINS'])) {
                $this->addOption("JOINS");
            }

            # Save the required data
            $data = array(
                $table_name,
                $connecting_base_table_field,
                $join_comparing_operator,
                $joining_connecting_table_field,
                $INNER_OR_FULL_OUTER_OR_LEFT_OR_RIGHT
            );

            # Build the join clause option builder
            $joinClauseStatementOptionBuilder = new JoinClauseStatementOptionBuilder();

            # Build the current where clause
            $option = $joinClauseStatementOptionBuilder->build(
                $this->getOptions()['JOINS'], $data
            );

            # If they option is not valid (empty), end
            if ($option == "") {
                return $this;
            }

            # Add the option to the collection
            $this->addOptionValueInArray("JOINS", $option);

            # Return this for method chaining
            return $this;
        }

        /**
         * Executes the query at the end of the method chain
         *
         * The function does not have the required options or should not execute
         */
        public final function __destruct()
        {
            /*
             * When the options have been build before we dont need to call it
             */
            if (!empty($this->options)) {
                $this->buildOptions();
            }

            /*
             * When the query should not execute yet, or doesn't have the required options
             * stop the function
             */
            if (!$this->shouldExecute || !$this->hasRequiredOptions()) {
                return;
            }

            $this->buildPDOStatement();
        }

        /**
         *
         */
        private function hasRequiredOptions() : bool
        {
            # Loop through all the require options
            # TODO Implement this when there are queries with required options
            /*foreach ($this->requiredOptions as $requiredOption) {
                # If required option is set in the current options, all good
                # If required option is set but no valid entries, no good
                if (isset($this->options[$requiredOption]) || !empty($this->options[$requiredOption])) {
                    continue;
                }

                # If not, return false
                return false;
            }*/

            # If all found, return true
            return true;
        }

        /**
         * @return \PDOStatement
         */
        private function buildPDOStatement() : \PDOStatement
        {
            # End the statement with a ";" character
            $statementText = $this->currentStatement->getStatementText() . ";";

            # Execute the statement if it has the required options
            $executedPDOStatement =
                $this->database->executePrepared($statementText, $this->currentStatement->getParameters());

            # Return it
            return $executedPDOStatement;
        }

        /**
         * Build all the options in a correct order
         */
        private function buildOptions()
        {
            # Add the join clauses
            if (isset($this->options["JOINS"])) {
                $option = "";
                # Add the where clauses to the current statement
                foreach ($this->options['JOINS'] as $joinClause) {
                    $option .= $joinClause;
                }

                # Add the option to the current statement
                $this->getCurrentStatement()
                     ->addOption($option);

                # Unset the WHERE clause options so it doesn't give errors
                unset($this->options['JOINS']);
            }

            # Add the where clauses
            if (isset($this->options["WHERE"])) {
                $option = "WHERE";

                # Add the where clauses to the current statement
                foreach ($this->options['WHERE'] as $whereClause) {
                    $option .= " " . $whereClause;
                }

                # Add the option to the current statement
                $this->getCurrentStatement()
                     ->addOption($option);

                # Unset the WHERE clause options so it doesn't give errors
                unset($this->options['WHERE']);
            }
        }

        /**
         * @return \basvandriel\Shelves\Database\Statement\Statement
         */
        protected function getCurrentStatement() : Statement
        {
            return $this->currentStatement;
        }

        /**
         *
         */
        public function getStatement() : string
        {
            /*
             * When the options have been build before we dont need to call it
             */
            if (!empty($this->options)) {
                $this->buildOptions();
            }

            # Return the statement text
            return $this->getCurrentStatement()
                        ->getStatementText();
        }

        /**
         * @return \PDOStatement
         */
        public function getPDOStatement() : \PDOStatement
        {
            # Build the options
            $this->buildOptions();

            # Return an empty PDO statement
            if ($this->shouldExecute == false || !$this->hasRequiredOptions()) {
                return new \PDOStatement();
            }

            # The query shouldn't execute again
            $this->shouldExecute = false;

            # Build the pdo statement
            return $this->buildPDOStatement();
        }

        /**
         * @param string $option_name
         */
        protected function addOption(string $option_name)
        {
            $this->options[$option_name] = array();
        }

        /**
         * @param string $option_name
         * @param string $option_value
         */
        protected function addOptionValueInArray(string $option_name, string $option_value)
        {
            $this->options[$option_name][] = $option_value;
        }

        /**
         * @return array
         */
        protected function getOptions() : array
        {
            return $this->options;
        }
    }