<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\Options\Option;

    use basvandriel\Shelves\Database\Statement\Data\TableNameAliasFactory;
    use basvandriel\Shelves\Database\Statement\Data\TableNameFactory;

    /**
     * TODO Document the purpose of the JoinClauseStatementOptionBuilder class
     *
     * @package   basvandriel\Shelves\Database\Statement\Options\Option
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class JoinClauseStatementOptionBuilder implements StatementOptionBuilder
    {

        /**
         * @param array $current_option
         * @param array $data
         *
         * @return mixed|string
         */
        public function build(array $current_option, array $data) : string
        {
            # Save the raw joining table name
            $joiningRawTableName = trim($data[0]);

            # Save the joining table name
            $joiningTableName = (new TableNameFactory())->buildTableName($joiningRawTableName);

            # Save the table name alias
            $joiningTableNameAlias = (new TableNameAliasFactory($joiningRawTableName))->buildForTableName();

            # Save the connecting base table field
            $connectingBaseTableField = $this->resolvePrefixedTableField("", trim($data[1]));

            # Save the condition operator trimmed
            $comparingOperator = trim($data[2]);

            # Save the joining connection table table field
            $joiningConnectingTableField = $this->resolvePrefixedTableField($joiningRawTableName, trim($data[3]));

            # Save the AND or OR operator trimmed to uppercase
            $joinType = strtoupper(trim($data[4]));

            # Save the valid operators
            $validJoinTypes = array(
                "INNER",
                "LEFT",
                "RIGHT",
                "FULL OUTER",
            );

            # Check if the join type is valid
            if (!in_array($joinType, $validJoinTypes)) {
                return "";
            }

            # Save a blank option value
            $option = "";

            # If there are already more WHERE clauses, add the AND or OR
            if (!empty($current_option)) {
                $option .= " ";
            }

            # Build the option
            $option .= "$joinType JOIN `$joiningTableName`";

            # Add the table alias
            if (!empty($joiningTableNameAlias)) {
                $option .= " AS $joiningTableNameAlias";
            }

            # Add the rest of the join clause
            $option .= " ON $connectingBaseTableField $comparingOperator $joiningConnectingTableField";

            return $option;
        }

        /**
         * @param string $table_name
         * @param string $table_field
         *
         * @return string
         */
        private function resolvePrefixedTableField(string $table_name, string $table_field)
        {
            $tablePrefixWithField = explode(".", $table_field);
            $tableAliasPrefixWithField = explode("::", $table_field);

            if (count($tablePrefixWithField) == 2) {
                # Check if size is table is prefixed by "." character
                $table_name = "`$tablePrefixWithField[0]`";
                $table_field = $tablePrefixWithField[1];
            } else if (count($tablePrefixWithField) != 2 && count($tableAliasPrefixWithField) == 2) {
                # Check if size is table is prefixed by "::" character
                $table_name = $tableAliasPrefixWithField[0];
                $table_field = $tableAliasPrefixWithField[1];
            } else if (!empty($table_name)) {
                # If there is no join and a table name alias set
                $tableAlias = (new TableNameAliasFactory($table_name))->buildForTableName();

                # Set the table name to the table alias if it exists
                $table_name = !empty($tableAlias) ? $tableAlias : "`$table_name`";
            }

            # Set the prefixed table name to the field only
            $prefixedTableField = "`$table_field`";

            # If the table name is empty
            if (!empty($table_name)) {
                $prefixedTableField = "$table_name.$prefixedTableField";
            }

            return $prefixedTableField;
        }
    }