<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\Options\Option;

    /**
     * TODO Document the purpose of the WhereClauseStatementOptionBuilder class
     *
     * @package   basvandriel\Shelves\Statement\Option
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class WhereClauseStatementOptionBuilder implements StatementOptionBuilder
    {

        /**
         * @param array $current_option
         * @param array $data
         *
         * @return mixed|string
         */
        public function build(array $current_option, array $data) : string
        {
            # Set the table name
            $tableName = $data[0];

            # Set the column name to the standard
            $columnName = $data[1];

            # Set the prepared statement parameter name
            $parameterName = $data[2];

            # Save the column name with possible table name
            $fullColumnName = $this->buildFullColumnName($tableName, $columnName, $tableName != "");

            # Save the condition operator trimmed
            $conditionOperator = trim($data[3]);

            # Save the AND or OR operator trimmed to uppercase
            $andOrOr = strtoupper(trim($data[4]));

            # Save the valid operators
            $validOperator = array(
                "=",
                ">",
                ">=",
                "IS",
                "IS NOT",
                "<",
                "<=",
                "LIKE",
                "!="
            );

            # Check if the condition operator is valid and if the AND or OR is valid
            if (!in_array($conditionOperator, $validOperator) || !in_array($andOrOr, array("AND", "OR"))) {
                return "";
            }

            # Save the option string
            $option = "";

            # If there are already more WHERE clauses, add the AND or OR
            if (!empty($current_option)) {
                $option .= $andOrOr . " ";
            }

            # Build it
            $option .= $fullColumnName . " " . $conditionOperator . ' ' . ":" . $parameterName;

            # Return it
            return $option;
        }

        /**
         * @param string $table_name
         * @param string $column_name
         *
         * @param bool   $prefixed_table_name
         *
         * @return string
         *
         * TODO Get rid of table name prefixing when joins are not there
         */
        public function buildFullColumnName(string $table_name, string $column_name, bool $prefixed_table_name = false)
        {
            $fullColumnName = "";

            # If the table name should be prefixed add it to it
            if ($prefixed_table_name) {
                $fullColumnName .= "`" . $table_name . "`.";
            }

            # Save the column name
            $column_name = "`" . $column_name . "`";

            # Paste the column name to the string
            $fullColumnName .= $column_name;

            # Return the full column name
            return $fullColumnName;
        }
    }