<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\Options;

    use basvandriel\Shelves\Database\Database;
    use basvandriel\Shelves\Database\Statement\Data\ColumnNameFactory;
    use basvandriel\Shelves\Database\Statement\Data\Parameter\ParameterNameResolver;
    use basvandriel\Shelves\Database\Statement\Data\TableNameFactory;
    use basvandriel\Shelves\Database\Statement\Options\Option\WhereClauseStatementOptionBuilder;
    use basvandriel\Shelves\Database\Statement\Statement;

    /**
     * TODO Document the purpose of the TableAffectingStatementOptionsBuilder class
     *
     * @package   basvandriel\Shelves\Statement
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class TableAffectingStatementOptionsBuilder extends StatementOptionsBuilder
    {
        /**
         * @var string $currentTableName The table name we're currently affecting
         */
        private $currentTableName;

        /**
         * TableAffectingStatementOptionsBuilder constructor.
         *
         * @param \basvandriel\Shelves\Database\Database            $database
         * @param \basvandriel\Shelves\Database\Statement\Statement $current_statement
         * @param array                                             $required_options
         * @param string                                            $current_table_name
         */
        public function __construct(Database $database, Statement $current_statement, $required_options = array(), string $current_table_name = "")
        {
            # Call the parents constructor
            parent::__construct($database, $current_statement, $required_options);

            # Set the table name we're using
            $this->currentTableName = $current_table_name;
        }


        /**
         * @param string $column_name
         * @param        $condition_operator
         *
         * @param mixed  $value
         *
         * @param string $AND_or_OR
         *
         * @return $this
         */
        public final function where(string $column_name, string $condition_operator, string $value, string $AND_or_OR = "AND")
        {
            # Create the array key if it doesn't exist
            if (!isset($this->getOptions()['WHERE'])) {
                $this->addOption("WHERE");
            }

            # Generate the table name for possible prefixing
            $tableName = (new TableNameFactory())->buildTablePrefix($this->currentTableName, $column_name);

            # Generate the column name with possibly the prefix of the table
            $column_name = (new ColumnNameFactory())->build($column_name);

            # Generate the prepared statement parameter name
            $parameterName = (new ParameterNameResolver())->build($tableName, $column_name);

            # Get the current statement
            $statement = $this->getCurrentStatement();

            # Fix parameter name conflicts
            if ($statement->getParameterCount($parameterName) >= 1) {
                $statement->increaseParameterCount($parameterName);

                $parameterName .= $statement->getParameterCount($parameterName);
            }


            # Save the option data
            $data = array(
                $tableName,
                $column_name,
                $parameterName,
                $condition_operator,
                $AND_or_OR,
            );

            # Create the where clause builder
            # TODO Use the already set column anme and table name here
            # TODO When the prefixing table isnt set, do not use table name
            $whereClauseStatementOptionBuilder = new WhereClauseStatementOptionBuilder();

            # Build the current where clause
            $option = $whereClauseStatementOptionBuilder->build(
                $this->getOptions()['WHERE'], $data
            );

            # If they option is not valid (empty), end
            if ($option == "") {
                return $this;
            }

            # Add the parameter with value to the collection
            $statement->addParameter($parameterName, $value);

            # Add the option to the collection
            $this->addOptionValueInArray("WHERE", $option);

            # Return $this for method chaining
            return $this;
        }
    }