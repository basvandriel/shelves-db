<?php
    /**
     * Created by IntelliJ IDEA.
     * User: basvandriel
     * Date: 15-6-17
     * Time: 13:44
     */

    namespace basvandriel\Shelves\Database\Statement;

    use basvandriel\Shelves\Database\Statement\Data\ColumnNameFactory;
    use basvandriel\Shelves\Database\Statement\Data\TableNameFactory;

    class UpdateStatementResolver implements StatementResolver
    {

        /**
         * @param string $table_name
         * @param array  $data
         *
         * @return string
         */
        public function resolve(string $table_name, array $data) : string
        {
            # Sanitize the table name
            $table_name = trim($table_name);

            # Start the statement
            $statement = /** @lang text */
                'UPDATE `' . $table_name . '`';

            # Save the update values into a variable
            $parameters_by_column = $data['parameters'];

            # Check if the options are empty  or is not associative
            if (empty($parameters_by_column)) {
                return "";
            }

            # Add the SET To the statement
            $statement .= " SET ";

            # Add the values to the statement
            foreach ($parameters_by_column as $column_name => $parameters) {
                # Get the parameter name from the parameter
                $parameter_name = key($parameters);

                # Get table name which is prefixed
                $tableName = (new TableNameFactory())->buildTablePrefix($table_name, $column_name);

                # Get the column name from the parameter
                $column_name = (new ColumnNameFactory())->build($column_name);

                $fullColumnName = "`$column_name`";
                # The table name prefix
                if (!empty($tableName)) {
                    $fullColumnName = "`$table_name`.`$column_name`";
                }

                # Add the column name to the statement
                $statement .= "$fullColumnName = :$parameter_name";
            }

            return $statement;
        }
    }