<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement;

    /**
     * TODO Document the purpose of the Statement class
     *
     * @package   basvandriel\Shelves\Statement
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class Statement
    {
        /**
         * @var string $statementText The statement text we're going to execute
         */
        private $statementText;

        /**
         * @var array $parameters The parameter names with value of the query which is getting build
         */
        private $parameters;

        /**
         * @var array $parameterNameCounts The parameter name counts to make sure no
         *                                 conflicts exists
         */
        private $parameterNameCounts;

        /**
         * Statement constructor.
         *
         * @param string $statementText
         *
         * @internal param array $parameters
         */
        public function __construct(string $statementText)
        {
            # Set the statement text
            $this->statementText = $statementText;

            # Set the parameters
            $this->parameters = array();

            # Set the parameter name counts
            $this->parameterNameCounts = array();
        }

        /**
         * @param string $statement_option
         */
        public function addOption(string $statement_option)
        {
            if (empty($statement_option)) {
                return;
            }
            $this->statementText .= " $statement_option";
        }

        /**
         * @param string $name
         * @param string $value
         *
         * @return void
         */
        public function addParameter(string $name, string $value)
        {
            # Sets the parameter with the value
            $this->parameters[$name] = $value;

            # Increase the counter
            $this->increaseParameterCount($name);
        }

        /**
         * @param string $parameter_name
         */
        public function increaseParameterCount(string $parameter_name)
        {
            if (!isset($this->parameterNameCounts[$parameter_name])) {
                $this->parameterNameCounts[$parameter_name] = 0;
            }

            # Increase the parameter count
            $this->parameterNameCounts[$parameter_name]++;
        }

        /**
         * @param string $parameter_name
         *
         * @return int
         */
        public function getParameterCount(string $parameter_name) : int
        {
            if (!isset($this->parameterNameCounts[$parameter_name])) {
                return 0;
            }

            return $this->parameterNameCounts[$parameter_name];
        }


        /**
         * @return string
         */
        public function getStatementText() : string
        {
            return $this->statementText;
        }

        /**
         * @return array The parameter names with value of the query which is getting build
         */
        public function getParameters() : array
        {
            return $this->parameters;
        }
    }