<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement\Data;

    /**
     * TODO Document the purpose of the TableNameAliasFactory class
     *
     * @package   basvandriel\Shelves\Database\Statement\Data
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class TableNameAliasFactory
    {
        /**
         * @var string $tableName
         */
        private $tableName;

        /**
         * TableNameAliasFactory constructor.
         *
         * @param string $table_name
         */
        public function __construct(string $table_name)
        {
            $this->tableName = $table_name;
        }

        /**
         * @return string
         */
        public function buildForTableName() : string
        {
            # Start as an empty alias
            $alias = "";

            # Explode the table name by the ":" character
            $exploded = explode(":", $this->tableName);

            # If there are 2 elements return the second element
            if (count($exploded) == 2) {
                $alias = $exploded[1];
            }

            # Return the alias
            return $alias;
        }
    }