<?php

    namespace basvandriel\Shelves\Database\Statement\Data\Parameter;

    class ParameterNameResolver
    {
        /**
         *
         * @param string $prefixed_table_name
         * @param string $column_name
         *
         * @return string
         */
        public function build(string $prefixed_table_name, string $column_name)
        {
            # The standard parameter name is the colum name
            $parameterName = $column_name;

            # Add the prefixed table name if it exists
            if ($prefixed_table_name !== "") {
                $parameterName = $prefixed_table_name . ucfirst($parameterName);
            }

            # Return the parameter name
            return $parameterName;
        }
    }