<?php
    /**
     * Created by IntelliJ IDEA.
     * User: basvandriel
     * Date: 13-6-17
     * Time: 13:13
     */

    namespace basvandriel\Shelves\Database\Statement\Data;


    class ColumnNameFactory
    {
        /**
         * @param string $column_name
         *
         * @return string
         */
        public function build(string $column_name) : string
        {
            # Save the full column name without the "`" around it
            $columnName = trim($column_name);

            # Save the joining table name and column name
            $joiningTableAndColumnName = explode(".", trim($column_name));

            # If there is a table prefixed before the column name
            if (count($joiningTableAndColumnName) == 2) {
                # Set the column name
                $columnName = $joiningTableAndColumnName[1];
            }

            return trim($columnName);
        }
    }