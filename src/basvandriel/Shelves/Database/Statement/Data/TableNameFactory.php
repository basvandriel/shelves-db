<?php
    /**
     * Created by IntelliJ IDEA.
     * User: basvandriel
     * Date: 13-6-17
     * Time: 13:33
     */

    namespace basvandriel\Shelves\Database\Statement\Data;


    class TableNameFactory
    {
        /**
         * TODO Only show table name prefix when
         * - There are joins
         *
         * @param string $currentTableName
         * @param string $column_name
         *
         * @return string
         */
        public function buildTablePrefix(string $currentTableName, string $column_name = "") : string
        {
            # Set the table name empty by default
            # No column gets prefixed by default
            $tableName = "";

            # Save the joining table name and column name
            $joiningTableAndColumnName = explode(".", $column_name);

            # If there is a table prefixed before the column name
            if (count($joiningTableAndColumnName) == 2) {

                /*
                 * TODO Check if the join exists for the prefixed table name
                 *
                 * $joiningTableAndColumnName[0] != $currentTableName
                 */
                # Set the table name to the prefixed table name
                # If the prefixed column name is the current table name, set it aswell
                $tableName = $joiningTableAndColumnName[0];
            }

            return trim($tableName);
        }

        /**
         * @param string $raw_table_name
         *
         * @return string
         */
        public function buildTableName(string $raw_table_name)
        {
            # Explode it
            $tableNameWithAlias = explode(":", $raw_table_name);

            # If there is a table alias
            if (count($tableNameWithAlias) == 2) {
                $raw_table_name = $tableNameWithAlias[0];
            }

            # Return it
            return $raw_table_name;
        }
    }