<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database\Statement;

    /**
     * TODO Document the purpose of the StatementObjectFactory class
     *
     * @package   basvandriel\Shelves\Database\Statement
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class PDOStatementFactory
    {
        /**
         * @var \PDO $pdoThe PDO connection object
         */
        private $pdo;

        /**
         * StatementObjectFactory constructor.
         *
         * @param \PDO $pdo The PDO connection object
         */
        public function __construct(\PDO $pdo)
        {
            $this->pdo = $pdo;
        }

        /**
         * @param string $statement
         *
         * @return \PDOStatement
         */
        public function build(string $statement) : \PDOStatement
        {
            # Sanitize the statement from special characters
            $statement = (new StatementSanitizer())->sanitizeStatement($statement);

            # Build the statement object
            $statementObject = $this->pdo->query($statement);

            return $statementObject;
        }

        /**
         * @param string $statement
         * @param array  $parameters
         *
         * @return \PDOStatement
         */
        public function buildPrepared(string $statement, array $parameters) : \PDOStatement
        {
            # Sanitize the statement from special characters
            $statement = (new StatementSanitizer())->sanitizeStatement($statement);

            # Save the PDO statement object
            $statementObject = $this->pdo->prepare($statement);

            # Map the prepared statement parameters
            $this->mapPreparedStatementParameters($statementObject, $parameters);

            # Execute the prepared statement
            $statementObject->execute();

            return $statementObject;
        }

        /**
         * @param \PDOStatement $PDOStatement
         * @param array         $parameters
         */
        private function mapPreparedStatementParameters(\PDOStatement &$PDOStatement, array $parameters)
        {
            # Save the possible types for matching the types
            $parameterTypes = array(
                'boolean'  => \PDO::PARAM_BOOL,
                'int'      => \PDO::PARAM_INT,
                'double'   => \PDO::PARAM_INT,
                'string'   => \PDO::PARAM_STR,
                'resource' => \PDO::PARAM_LOB,
                'null'     => \PDO::PARAM_NULL,
            );

            # Loop through the parameters for
            foreach ($parameters as $parameter => &$value) {
                # Check if the parameter is found in the query
                $parameterInString = strpos($PDOStatement->queryString, ":$parameter") !== false;

                # Parameter name should be a string or value can't be an array
                if ((!is_string($parameter) && !is_array($value)) || !$parameterInString) {
                    continue;
                }

                # Save the value data type
                $dataType =
                    isset($parameterTypes[gettype($value)]) ? $parameterTypes[gettype($value)] : \PDO::PARAM_STR;

                # Bind each found parameter
                $PDOStatement->bindParam(":$parameter", $value, $dataType);
            }
        }
    }