<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database;

    use basvandriel\Shelves\Database\Statement\Data\ColumnNameFactory;
    use basvandriel\Shelves\Database\Statement\Data\Parameter\ParameterNameResolver;
    use basvandriel\Shelves\Database\Statement\Data\TableNameFactory;
    use basvandriel\Shelves\Database\Statement\DeleteStatementResolver;
    use basvandriel\Shelves\Database\Statement\Options\TableAffectingStatementOptionsBuilder;
    use basvandriel\Shelves\Database\Statement\PDOStatementFactory;
    use basvandriel\Shelves\Database\Statement\Statement;
    use basvandriel\Shelves\Database\Statement\UpdateStatementResolver;


    /**
     * TODO Document the purpose of the Database class
     *
     * @package   basvandriel\Shelves
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class Database
    {
        /**
         * @var \PDO $connection The PDO connection object
         */
        private $connection;

        /**
         * Database constructor.
         *
         * @param \PDO $connection
         */
        public function __construct(\PDO $connection)
        {
            # Get the PDO object
            $this->connection = $connection;

            $driver = $this->connection->getAttribute(\PDO::ATTR_DRIVER_NAME);
        }

        /**
         * @param string $statement The statement to execute
         *
         * @return \PDOStatement The result of the statement
         */
        public function execute(string $statement) : \PDOStatement
        {
            # Initialize the factory with the PDO object
            $factory = new PDOStatementFactory($this->connection);

            # Build the statement and return it
            return $factory->build($statement);
        }

        /**
         * Executes a prepared statement with it's parameters
         *
         * @param string $statement  The statement text to execute
         * @param array  $parameters The parameters for the statement
         *
         * @return \PDOStatement The result of the statement
         */
        public function executePrepared(string $statement, array $parameters)
        {
            # Initialize the factory with the PDO object
            $factory = new PDOStatementFactory($this->connection);

            # Build the statement and return it
            return $factory->buildPrepared($statement, $parameters);
        }

        /**
         * <p>Deletes rows from a specific existing table name</p>
         * <br>
         * <p><b>CAUTION</b>: When you use this function without adding a where clause
         *          all the records will be deleted in the table</p>
         *
         * @param string $table_name The table where to delete rows from
         *
         * @return TableAffectingStatementOptionsBuilder The object for method chaining statement options
         */
        public function deleteFrom(string $table_name) : TableAffectingStatementOptionsBuilder
        {
            # Build the statement
            $statementText = (new DeleteStatementResolver())->resolve($table_name, array());

            # Build the statement
            $statement = new Statement($statementText);

            # Build the statement options builder object
            $statementOptionsBuilder = new TableAffectingStatementOptionsBuilder(
                $this, $statement, array(), $table_name
            );

            # Return the statement options connector for method chaining
            return $statementOptionsBuilder;
        }

        /**
         *
         *
         * CAUTION: When you use this function without adding a where clause
         *          all the records will be updated in the table
         *
         * @param string $table_name The table where to update data
         * @param array  $values     The update data
         *
         * @return TableAffectingStatementOptionsBuilder
         */
        public function update(string $table_name, array $values) : TableAffectingStatementOptionsBuilder
        {
            # Create an array for saving the parameters
            $parameters = array();

            # Resolve the table values into parameters
            foreach ($values as $column_name => $value) {
                # Save the table name
                $valueTableName = (new TableNameFactory())->buildTablePrefix($table_name, $column_name);

                # Save the column name
                $valueColumnName = (new ColumnNameFactory())->build($column_name);

                # Resolve the parameter name from the table name and column name
                $parameterName = (new ParameterNameResolver())->build(
                    $valueTableName, $valueColumnName
                );

                # Save it
                $parameters[$column_name] = array($parameterName => $value);
            }

            # Instantiate the update statement factory
            $updateStatementResolver = new UpdateStatementResolver();

            # Build the update statement
            $statementText = $updateStatementResolver->resolve($table_name, array('parameters' => $parameters));

            # Build the statement object
            $statement = new Statement($statementText);

            # Add the parameters to the statement
            foreach (array_values($parameters) as $parameter) {
                # Save the parameter name
                $parameterName = key($parameter);

                # Add the parameter to the statement
                $statement->addParameter($parameterName, $parameter[$parameterName]);
            }

            # Return the statement options connector for method chaining options
            return new TableAffectingStatementOptionsBuilder(
                $this, $statement, array(), $table_name
            );
        }
    }