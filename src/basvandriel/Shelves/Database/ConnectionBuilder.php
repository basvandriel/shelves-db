<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    namespace basvandriel\Shelves\Database;

    use PDO;

    /**
     * Builds a new database connection to a database
     *
     * @package   basvandriel\Shelves\Database
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/shelves
     */
    class ConnectionBuilder
    {
        /**
         * @var string $driver
         */
        private $driver;

        /**
         * DatabaseServerConnectionFactory constructor.
         *
         * @param string $driver
         *
         * @throws \PDOException
         */
        public function __construct(string $driver)
        {
            if (empty($driver)) {
                throw new \PDOException("The driver and name must be specified");
            }
            # Set the driver
            $this->driver = $driver;
        }


        /**
         * @param array $credentials
         *
         * @param array $options
         *
         * @return \PDO
         *
         * @throws \PDOException
         */
        public function build(array $credentials, array $options = array()) : \PDO
        {
            # All the required fields
            $requiredFields = array(
                'host',
                'database',
                'username',
            );

            # Check for the required fields
            foreach ($requiredFields as $requiredField) {
                if (isset($credentials[$requiredField])) {
                    continue;
                }
                throw new \PDOException('The ' . $requiredField . ' must be set.');
            }
            # Set the host to the array element
            $host = $credentials['host'];

            # Set the database name  to the array element
            $database = $credentials['database'];

            # Set the username to the array element
            $username = $credentials['username'];

            # Set the password to empty when it's not defined
            $password = $credentials['password'] ?? "";

            # Set the port to 3306 by default it's not defined
            $port = $credentials['port'] ?? "3306";

            # Set the charset to 'UTF-8' by default when it's not defined
            $charset = $credentials['charset'] ?? "UTF8";

            # Build the DSN
            $dsn = $this->resolveDSN($this->driver, $database, $host, $port, $charset);

            # Set the default options with the user defined ones
            $options = array_merge(
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ), $options
            );

            # Build a PDO object
            return new \PDO($dsn, $username, $password, $options);
        }

        /**
         * @param string $driver
         * @param string $database
         * @param string $host
         * @param int    $port
         * @param string $charset
         *
         * @return string
         */
        private function resolveDSN(string $driver, string $database, string $host, int $port, string $charset)
        {
            # Build the dsn
            $dsn = "$driver:dbname=$database;host=$host;port=$port;charset=$charset;";

            # Return it
            return $dsn;
        }
    }